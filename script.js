$(document).ready(function() { 

    $("#sign-up").validate({
        rules: {
            fname: {
                required: true,
                minlength: 4
            },
            lname: {
                required: true,
                minlength: 4
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            },
            day: {
                required: true
            },
            month: {
                required: true
            },
            year: {
                required: true,
                validBirthday: true
            },
            gender: {
                required: true,
                
            } 

        }, 
         messages: {
            fname: {
                required: "First name is required",
                minlength: "First name must be at least 4 characters long"
            },
            lname: {
                required: "Last name is required",
                minlength: "Last name must be at least 4 characters long"
            },
            email: {
                required: "Email is required",
                email: "Please enter a valid email address"
            },
            password: {
                required: "Password is required",
                minlength: "Password must be at least 6 characters long"
            },
            day: {
                required: "Please select a day"
            },
            month: {
                required: "Please select a month"
            },
            year: {
                required: "Please select a year",
               
            }
        },
      
    });
});
